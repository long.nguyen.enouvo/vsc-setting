# VSC-setting

{
    "editor.tabSize": 2,
    "editor.detectIndentation": true,
    "prettier.singleQuote": true,
    "editor.tabCompletion": true,
    // Use ‘prettier-eslint’ instead of ‘prettier’. Other settings will only be fallbacks in case they could not be inferred from eslint rules.
    "prettier.eslintIntegration": true,
    "window.zoomLevel": 0,
    "[typescript]": {
        "editor.formatOnSave": false,
    },
    "eslint.autoFixOnSave": true,
    "eslint.validate": [
        "javascript",
        "javascriptreact",
        {
            "language": "typescript",
            "autoFix": true
        },
        {
            "language": "typescriptreact",
            "autoFix": true
        }
    ],
    "tslint.autoFixOnSave": true,
    "[javascript]": {
        "editor.formatOnSave": true
    },
    "[python]": {
        "editor.tabSize": 4,
        "editor.formatOnPaste": true
    },
    "[html]": {
        "editor.tabSize": 4,
    },
    "editor.fontSize": 14,
    // "typescript.format.insertSpaceAfterFunctionKeywordForAnonymousFunctions": true,
    "javascript.format.insertSpaceAfterFunctionKeywordForAnonymousFunctions": true,
    "terminal.integrated.rendererType": "dom",
    "javascript.updateImportsOnFileMove.enabled": "always",
    "explorer.confirmDragAndDrop": false,
    "scssLint.runOnTextChange": true,
    "scssLint.showHighlights": true,
    "editor.codeActionsOnSaveTimeout": 300,
    "prettier.stylelintIntegration": true,
    "sassFormat.indent": 2,
    "prettier.tabWidth": 2,
    "workbench.iconTheme": "material-icon-theme",
    "explorer.confirmDelete": false,
    "typescript.updateImportsOnFileMove.enabled": "always",
    "python.linting.pylintEnabled": false,
    "python.linting.pylintArgs": [
        "--load-plugins",
        "pylint_flask"
    ],
    "python.linting.flake8Enabled": true,
    "python.linting.flake8Args": [
        "--ignore=E24,W504",
        "--verbose"
    ]
}